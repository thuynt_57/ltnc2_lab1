/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing
 *
 * Description : Ex5 of lab1
 * Task		   : Tinh so thu k cua day so fibonaxi: 0 1 1 2 3 5 8 
 				Su dung de qui va khong de quy
 				
 *			
 
 * Date        : 11/4/2015
 *
 */
 
 
#include <iostream>
#include <string.h>
#define MAX 100

using namespace std;

int fibonaxi_recursive ( int k ) {
	if ( k == 1 ) return 0;
	if ( k == 2 ) return 1;
	return fibonaxi_recursive ( k - 1 ) + fibonaxi_recursive ( k - 2 );
}

int fibonaxi_non_recursive ( int k ) {
	int a1, a2, a3;
	a1 = 0;
	a2 = 1;
	if ( k == 1 ) return a1;
	if ( k == 2 ) return a2;
	for ( int i = 3; i <= k; i++) {
		a3 = a1 + a2;
		a1 = a2;
		a2 = a3; 
	}
	return a3;
}

int main () {
	int k;
	cout << "Enter the index u want to know the value in fibonaxi sequence: ";
	cin >> k;
	cout << "Value at index " << k << " using recursive is: " << fibonaxi_recursive ( k ) << endl;
	cout << "Value at index " << k << " not using recursive is: " << fibonaxi_non_recursive ( k ) << endl; 
	return 0;
}
