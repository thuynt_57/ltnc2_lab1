/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing
 *
 * Description : Ex4 of lab1
 * Task		   : Mot nguoi co tai khoan tiet kiem o ngan hang va gui vao X dong voi lai suat laf 7%/ nam, ky han 1 thang (tuc la cu mot thang thi tien tang len 7%/12)
 				Hoi sau N thang M ngay thi so tien cua nguoi ay rut ra la bao nhieu (ca goc va lai). Neu so ngay khong du 1 thang thi coi nhu khong tinh lai.
 *			
 
 * Date        : 11/4/2015
 *
 */
 
 
#include <iostream>
#include <string.h>
#define MAX 100
#define interestRate 0.07

using namespace std;

float totalMoney ( float X, int N, int M ) {
	float totalMoney;
	int month = N + (int) (M/12);
	totalMoney = X + X * interestRate / 12 * month;
	return totalMoney;
}

int main () {
	float X;
	int N, M;
	cout << "Enter the mount of money u gave: ";
	cin >> X;
	cout << "Enter the month: ";
	cin >> N;
	cout << "Enter the day: ";
	cin >> M;
	cout << "Now u have " << totalMoney ( X, N, M ) << endl;
	return 0;
}
