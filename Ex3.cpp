/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing
 *
 * Description : Ex3 of lab1
 * Task		   : Nhap vao mot day n so tu nhien bat ky, hay kiem tra xem trong nhung day ay co bao nhieu cap chu so ma so nay la boi so cua so kia.
 *			
 
 * Date        : 11/4/2015
 *
 */
 
 
#include <iostream>
#include <string.h>
#define MAX 100

using namespace std;

int countMultiplePair ( int a[], int n) {
	int num = 0;
	for ( int i = 0; i < n - 1; i++ ) {
		for ( int j = i + 1; j < n; j++ ) { 
			if ( a[i] == a[j] ) 
				num ++;
			else if ( a[i] % a[j] == 0 || a[j] % a[i] == 0 ) num++; 
			
		}
	}
	return num;
}

int main () {
	int n, a[MAX];
	cout << "Enter the length of list: ";
	cin >> n;
	cout << "Enter the list of int number: ";
	for ( int i = 0; i < n; i++) {
		cin >> a[i];
	}
	cout << "Number of multiple pair: " << countMultiplePair ( a, n);
	return 0;
}
