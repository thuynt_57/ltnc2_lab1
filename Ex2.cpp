/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing
 *
 * Description : Ex2 of lab1
 * Task		   : Nhap vao xau ky tu, dem xem xau nay chua bao nhieu phan tu, voi dieu kien cac tu cach nhau boi dau cach
 *			
 
 * Date        : 11/4/2015
 *
 */
 
 
#include <iostream>
#include <string.h>
#define MAX 100

using namespace std;

// remove all space chat at the beginning of the string
char *removeSpace( char a[] ) {
    int i=0;
    while(a[i]==32)
        i++;
    return a+i;
}

int countWord( char a[] ) {
      char c[MAX];
      strcpy( c, removeSpace( a ) );
      int i = 0
	  int count = 0;
      do {
		if( c[i] == ' ' && c[i+1] != ' ' && c[i+1] != '\0')
		count++;
		i++;
	  } while(c[i]!='\0');
 
      return count+1; 
}

int main () {
	char *a;
	a = new char[MAX];
	cout << "Enter your string: ";
	cin.getline(a, 100);
	cout << "Num of word is: " << countWord(a);
	return 0;
}


