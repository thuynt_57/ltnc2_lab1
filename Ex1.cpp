/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing
 *
 * Description : Ex1 of lab1
 * Task		   : Cho vi du ve vao ra doi voi kieu du lieu co ban su dung cin, cout, long, int, short, float, double, char. 
 *				Cho vi du ra vao doi voi xau ki tu kieu char*, nhap tung tu, nhap ca cau
 
 * Date        : 11/4/2015
 *
 */
 
 
#include <iostream>
#define MAX 100

using namespace std;
 
 int main () {
 	int intVar;
 	long longVar;
	short shortVar;
	float floatVar;
	double doubleVar;
	char charVar;
	char *charVar1, *charVar2;
	charVar1 = new char[MAX];
	charVar2 = new char[MAX];

	cout << "Enter an int number: "; cin >> intVar; 		cout << "Your int var is: " << intVar << endl;
	cout << "Enter a long number: "; cin >> longVar; 		cout << "Your long var is: " << intVar << endl;	
	cout << "Enter a short number: "; cin >> shortVar; 		cout << "Your short var is: " << shortVar << endl;
	cout << "Enter a float number: "; cin >> floatVar; 		cout << "Your float var is: " << floatVar << endl;
	cout << "Enter a double number: "; cin >> doubleVar; 	cout << "Your double var is: " << doubleVar << endl;
	cout << "Enter a char: "; cin >> charVar; 				cout << "Your charVar var is: " << charVar << endl;	
	
	cout << "Enter a word: "; 	
	cin.ignore(1); 
	cin >> charVar1;				
	cout << "Your word is: " << charVar1 << endl;
	cout << "Enter a sentence: ";
	cin.ignore(1);
	cin.getline( charVar2, 100 );	
	cout << "Your sentence is: " << charVar2 << "\n";
 	return 0;
 }
 
 
 
 
